from itertools import combinations

from scipy.spatial import distance
from gensim.models.keyedvectors import Word2VecKeyedVectors

from data import load_odd_one_out


def main():
    # Load the pretrained embeddings
    pretrained_word_embeddings_file = "./pretrained_embeddings/glove-wiki-gigaword-50.txt"
    data = load_odd_one_out()
    pretrained_embeddings = Word2VecKeyedVectors.load_word2vec_format(
        pretrained_word_embeddings_file
    )

    # Iterate over the examples
    for example in data:
        # TODO: Implement the odd one out logic
        odd_one = pretrained_embeddings.doesnt_match(example)
        print(f"Odd one out in {example} is {odd_one}.")
        print(10 * "-")


if __name__ == "__main__":
    main()
